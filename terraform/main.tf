provider "aws" {
  region = "us-east-1"
	access_key = ""
	secret_key = ""
}

resource "aws_security_group" "allow_ssh" {
  name_prefix = "allow_ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "app" {
  ami           = "ami-04b70fa74e45c3917"
  instance_type = "t2.micro"
  key_name      = var.key_name
  security_groups = [aws_security_group.allow_ssh.name]

  tags = {
    Name = "crud-api-instance"
  }
}

variable "key_name" {
  description = "Name of the SSH key pair"
}

variable "private_key_path" {
  description = "Path to the private key"
}

output "instance_ip" {
  value = aws_instance.app.public_ip
}
