pipeline {
    agent any

    environment {
        NEXUS_REPO_URL = '172.105.154.127:5000/repository/docker-hosted/'
        NEXUS_REPO_CREDENTIALS = credentials('nexus-repo-credentials')
        AWS_KEY_NAME = 'testpair'
        SSH_PRIVATE_KEY_PATH = '/var/jenkins_home/.ssh/testpair.pem'
        AWS_ACCESS_KEY = ''
        AWS_SECRET_KEY = ''
    }

    stages {
        stage('Read SSH Public Key') {
            steps {
                script {
                    def publicKey = readFile("${SSH_PRIVATE_KEY_PATH}").trim()
                    writeFile file: 'terraform/ssh_key.pem', text: publicKey
                    env.PUBLIC_KEY = publicKey
                }
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    dockerImage = docker.build("my-crud-api:latest")
                }
            }
        }

        stage('Push Docker Image to Nexus') {
            steps {
                script {
                    docker.withRegistry("http://${NEXUS_REPO_URL}", 'nexus-repo-credentials') {
                        dockerImage.push()
                    }
                }
            }
        }

        stage('Create AWS Instance with Terraform') {
            steps {
                script {
                    sh '''
                    cd terraform
                    terraform init
                    terraform apply -auto-approve -var "key_name=${AWS_KEY_NAME}" -var "private_key_path=${SSH_PRIVATE_KEY_PATH}"
                    '''
                }
            }
        }

        stage('Setup and Deploy with Ansible') {
            steps {
                script {
                    sh '''
                    instance_ip=$(terraform -chdir=terraform output -raw instance_ip)
                    echo "[aws_instance]" > inventory
                    echo "${instance_ip}" >> inventory

                    sleep 20

                    ssh-keyscan -H ${instance_ip} >> /var/jenkins_home/.ssh/known_hosts
                    if [ $? -ne 0 ]; then
                        echo "ssh-keyscan failed"
                        exit 1
                    fi
                    echo "ssh-keyscan completed successfully"

					# Wait for the instance to be ready
                    echo "Waiting for the instance to be ready..."
                    sleep 60

					echo "Instance IP----------------: ${instance_ip}"
                    echo "Checking SSH connection..."
                    for i in {1..10}; do
                       ssh_check_output=$(ssh -o StrictHostKeyChecking=no -i ${SSH_PRIVATE_KEY_PATH} ubuntu@${instance_ip} 'echo SSH is up' 2>&1)
                        echo "${ssh_check_output}"
                        if echo "${ssh_check_output}" | grep -q 'SSH is up'; then
                            echo "SSH is up"
                            break
                        fi
                        echo "Waiting for SSH to be available... attempt ${i}"
                        sleep 10
                    done

                    ansible-playbook -i inventory -u ubuntu --private-key=${SSH_PRIVATE_KEY_PATH} ansible/setup-docker.yml | tee ansible_setup_output.txt
                    ansible-playbook -i inventory -u ubuntu --private-key=${SSH_PRIVATE_KEY_PATH} ansible/deploy-app.yml | tee ansible_deploy_output.txt

					# Print the Ansible output to console
                    echo "===== Ansible Setup Output ====="
                    cat ansible_setup_output.txt
                    echo "===== Ansible Deploy Output ====="
                    cat ansible_deploy_output.txt
                    '''
                }
            }
        }
    }

    post {
        always {
						archiveArtifacts artifacts: 'ansible_setup_output.txt, ansible_deploy_output.txt', allowEmptyArchive: true
            deleteDir()
        }
    }
}
