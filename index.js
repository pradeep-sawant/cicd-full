const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

mongoose.connect('mongodb://mongo:27017/crud', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const ItemSchema = new mongoose.Schema({
	name: String,
	description: String,
});

const Item = mongoose.model('Item', ItemSchema);

app.get('/', (req, res) => {
	res.send('Hello World');
});

// Create
app.post('/items', async (req, res) => {
	const item = new Item(req.body);
	await item.save();
	res.send(item);
});

// Read
app.get('/items', async (req, res) => {
	const items = await Item.find();
	res.send(items);
});

// Update
app.put('/items/:id', async (req, res) => {
	const item = await Item.findByIdAndUpdate(req.params.id, req.body, {
		new: true,
	});
	res.send(item);
});

// Delete
app.delete('/items/:id', async (req, res) => {
	await Item.findByIdAndDelete(req.params.id);
	res.send({ message: 'Item deleted' });
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
	console.log(`Server is running on port ${port}`);
});
